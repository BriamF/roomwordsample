package com.example.roomwordsample

import androidx.lifecycle.*
import kotlinx.coroutines.launch

class WordViewModel(private val repository: WordRepository) : ViewModel() {

    // Usando LiveData y almacenando en cache lo que devuelve allWords tiene varios beneficios:
    // - Se puede poder un observador en los datos (enlugar de sondear cambios) y solo actualizar
    //      la interfaz de usuario cuando los datos realmente cambian.
    // - El repositorio esta completamente separado de la interfaz de usuario mediante el ViewModel
    val allWords: LiveData<List<Word>> = repository.allWords.asLiveData()
    /**
     * Lanzamiento de una nueva corrutina para insertar los datos de forma sin bloqueo
     **/
    fun insert(word: Word) = viewModelScope.launch {
        repository.insert(word)
    }
}

class WordViewModelFactory(private val repository: WordRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WordViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return WordViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
