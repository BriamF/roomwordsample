package com.example.roomwordsample

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

//Declarar el DAO como un propiedad privada en el constructor
// Pasar el DAO en lugar de toda la base de datos, porque solamente se necesita
// acceso al DAO.
class WordRepository(private val wordDao: WordDao) {
    // Room ejecuta todas las consultas en un hilo separado
    // El flujo observado notificará al observer cuando los datos hayan cambiado

    val allWords: Flow<List<Word>> = wordDao.getAlphabetizedWords()

    // De manera predeterminada Room ejecuta "suspend queries" fuera del hilo principal,
    // por lo tanto, no es necesario implementar nada más para estar seguro de que no se esta
    // haciendo un trabajo de base de datos de larga ejecucción fuera del hilo principal.
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(word: Word) {
        wordDao.insert(word)
    }
}